package com.example.ssoapplication.config;

import com.example.ssoapplication.filter.MacaroonAuthenticationFilter;
import com.example.ssoapplication.provider.MacaroonAuthenticationProvider;
import com.example.ssoapplication.provider.OpenIDUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.openid.OpenIDAuthenticationFilter;
import org.springframework.security.openid.OpenIDAuthenticationProvider;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
            new AntPathRequestMatcher("/public/**")
    );

    private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

    private MacaroonAuthenticationProvider authProvider;

    public SecurityConfig(MacaroonAuthenticationProvider _provider) {
        this.authProvider = _provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(this.authProvider);
        auth.authenticationProvider(openIDAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .logout().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(restAuthenticationFilter(), OpenIDAuthenticationFilter.class)
                .addFilterBefore(openIDAuthenticationFilter(), AnonymousAuthenticationFilter.class)
                .httpBasic().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/google-login"))
                .and()
                .authorizeRequests().anyRequest().authenticated();
    }


    @Bean
    OpenIDAuthenticationProvider openIDAuthenticationProvider(){
        OpenIDAuthenticationProvider provider = new OpenIDAuthenticationProvider();
        provider.setUserDetailsService(new OpenIDUserDetailsService());
        return provider;
    }

    @Bean
    OpenIDAuthenticationFilter openIDAuthenticationFilter()  throws Exception {
        final OpenIDAuthenticationFilter filter = new OpenIDAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManager());
        return filter;
    }

    @Bean
    MacaroonAuthenticationFilter restAuthenticationFilter() throws Exception {
        final MacaroonAuthenticationFilter filter = new MacaroonAuthenticationFilter(PROTECTED_URLS);
        filter.setAuthenticationManager(authenticationManager());
        return filter;
    }
}
