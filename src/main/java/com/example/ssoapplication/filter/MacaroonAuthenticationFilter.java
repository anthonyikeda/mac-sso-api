package com.example.ssoapplication.filter;

import com.example.ssoapplication.provider.MacaroonAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

public class MacaroonAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private Logger log = LoggerFactory.getLogger(MacaroonAuthenticationFilter.class);

    private static final String MACAROON = "macaroon";

    public MacaroonAuthenticationFilter(final RequestMatcher requiresAuth) {
        super(requiresAuth);
    }

    /**
     * Goal here is mixed up...
     *
     * we should be verifying the username/password to generate the macaroon for the client. but it seems to be wanting
     * to use the macaroon to do the authentication....
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {

        log.debug("Attempting Authentication...");
        final String param = ofNullable(request.getHeader(AUTHORIZATION))
                .orElse(request.getParameter("t"));

        final String token = ofNullable(param)
                .filter((val) -> contains(val, "macaroon"))
                .map(value -> removeStart(value, MACAROON))
                .map(String::trim)
                .orElse(null);

        // Instead of throwing an exception here if the macaroon is not found, we want to filter down to the
        // OpenID filter to try at that level...
        if (token != null) {
            log.debug("Token is {}", token);
            final Authentication auth = new MacaroonAuthenticationToken(token, null);
            return getAuthenticationManager().authenticate(auth);
        } else {
            log.debug("No token found");
            throw new
                    BadCredentialsException("External system authentication failed");
        }
//        final Authentication auth = new UsernamePasswordAuthenticationToken(token, token);
//        return getAuthenticationManager().authenticate(auth);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

}
