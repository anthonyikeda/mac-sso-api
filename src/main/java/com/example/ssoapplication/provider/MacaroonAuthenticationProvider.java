package com.example.ssoapplication.provider;

import com.example.ssoapplication.config.MacaroonConfig;
import com.github.nitram509.jmacaroons.Macaroon;
import com.github.nitram509.jmacaroons.MacaroonsBuilder;
import com.github.nitram509.jmacaroons.MacaroonsVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class MacaroonAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private Logger log = LoggerFactory.getLogger(MacaroonAuthenticationProvider.class);

    private MacaroonConfig macaroonConfig;

    @Override
    public boolean supports(Class<?> authentication) {
        return MacaroonAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public MacaroonAuthenticationProvider(MacaroonConfig _config) {
        this.macaroonConfig = _config;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.debug("Performing additional authentication checks for {}", userDetails.getUsername());
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.debug("Retrieving user {} in provider", username);
        Object authToken = authentication.getCredentials();

        Macaroon macaroon = MacaroonsBuilder.deserialize((String) authToken);
        log.debug("Macaroons id: {}, Location: {}", macaroon.identifier, macaroon.location);

        MacaroonsVerifier verifier = new MacaroonsVerifier(macaroon);

        verifier.assertIsValid(macaroonConfig.getSecret());

        MacaroonUser user = new MacaroonUser();
        user.setUsername(macaroon.identifier);
        user.setEmailAddress(macaroon.location);
        user.setPassword(macaroon.signature);

        // Since the macaroon is verified the user is considered "enabled"
        // The LoginServer will determine the field from the database. If the user is disabled, it will not issue
        // the macaroon.
        user.setEnabled(true);
        return user;

    }
}