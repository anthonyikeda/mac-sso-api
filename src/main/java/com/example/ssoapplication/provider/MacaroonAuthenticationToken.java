package com.example.ssoapplication.provider;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class MacaroonAuthenticationToken extends AbstractAuthenticationToken {

    private String token;

    public MacaroonAuthenticationToken(String _token,
                                       Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.token = _token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
